// Package main counts 3 most prevalent colors in images, whose URLs are provided
// in text file and writes the result into a CSV file in a form of url,color,color,color.
package main

import (
	"bufio"
	"bytes"
	"encoding/csv"
	"fmt"
	"image"
	"image/jpeg"
	"image/png"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"runtime"
	"sort"
	"strconv"
	"sync"
	"time"
)

func init() {
	image.RegisterFormat("jpeg", "jpeg", jpeg.Decode, jpeg.DecodeConfig)
	image.RegisterFormat("png", "png", png.Decode, png.DecodeConfig)
}

func main() {
	start := time.Now()
	done := make(chan interface{})
	defer close(done)

	csvFile, err := os.Create("PixelColors.csv")
	if err != nil {
		log.Fatalf("failed creating file: %s", err)
	}
	defer func(csvFile *os.File) {
		err := csvFile.Close()
		if err != nil {
			log.Fatalf("error closing file: %s", err)
		}
	}(csvFile)
	csvWriter := csv.NewWriter(csvFile)

	urls := getURLs(done, "https://gist.githubusercontent.com/ehmo/e736c827ca73d84581d812b3a27bb132/raw/77680b283d7db4e7447dbf8903731bb63bf43258/input.txt")
	picts := getFiles(done, urls)
	topColors := make([]<-chan []string, numCPU)
	for i := 0; i < numCPU; i++ {
		topColors[i] = getTopColors(done, picts)
	}
	for topColor := range fanIn(done, topColors...) {
		_ = csvWriter.Write(topColor)
	}
	csvWriter.Flush()
	fmt.Printf("Analysis took: %v\n", time.Since(start))
}

// getURLs takes URL with URLs of images to process and returns channel of those URLs.
func getURLs(done <-chan interface{}, urls string) <-chan string {
	urlStream := make(chan string)
	go func() {
		defer close(urlStream)

		urlFile, err := http.Get(urls)
		if err != nil || urlFile.StatusCode != 200 {
			log.Fatalf("error downloading URLs file: %s", urls)
		}
		input := bufio.NewScanner(urlFile.Body)
		// In case of billions of URLs there won't be enough memory for this map...
		// visitedUrls := make(map[string]bool)
		for input.Scan() {
			url := input.Text()
			// if _, value := visitedUrls[url]; !value {
			// 	visitedUrls[url] = true
			select {
			case urlStream <- url:
			case <-done:
				return
			}
			// }
		}
	}()
	return urlStream
}

// getFiles takes channel of URLs of images and returns channel of struct containing
// binary data of these images and their URL.
func getFiles(done <-chan interface{}, urlStream <-chan string) <-chan pictFile {
	pictStream := make(chan pictFile, numCPU)
	go func() {
		defer close(pictStream)
		for url := range urlStream {
			resp, err := http.Get(url)
			if err != nil {
				fmt.Printf("error downloading image file: %s\n", url)
				continue
			}
			pictBuf, err := ioutil.ReadAll(resp.Body)
			_ = resp.Body.Close()
			if err != nil {
				fmt.Println(err)
			}
			picture := pictFile{
				pict: pictBuf,
				url:  url,
			}
			select {
			case <-done:
				return
			case pictStream <- picture:
			}
		}
	}()
	return pictStream
}

// getTopColors takes channel of struct containing binary data of images and their URL
// and returns channel of slice containing image URL and top three colors in hex.
func getTopColors(done <-chan interface{}, pictStream <-chan pictFile) <-chan []string {
	topColorsStream := make(chan []string)
	go func() {
		defer close(topColorsStream)
		for pict := range pictStream {
			imgReader := bytes.NewReader(pict.pict)

			imgCfg, _, err := image.DecodeConfig(imgReader)
			if err != nil {
				fmt.Printf("invalid image format on: %s\n", pict.url)
				continue
			}

			width := imgCfg.Width
			height := imgCfg.Height
			_, _ = imgReader.Seek(0, 0)
			img, _, err := image.Decode(imgReader)
			if err != nil {
				fmt.Printf("invalid image format on: %s\n", pict.url)
				continue
			}
			colorFreq := make(map[colorRGB]int)
			var pixelColor colorRGB

			for y := 0; y < height; y++ {
				for x := 0; x < width; x++ {
					pixelColor.r, pixelColor.g, pixelColor.b, _ = img.At(x, y).RGBA()
					colorFreq[pixelColor]++
				}
			}
			threeMostFrequent := getThreeMostFrequent(colorFreq)
			csvdata := make([]string, 4)
			csvdata[0] = pict.url
			for i, frequency := range threeMostFrequent {
				hexR := fmt.Sprintf("%02s", strconv.FormatUint(uint64(frequency.color.r/0x101), 16))
				hexG := fmt.Sprintf("%02s", strconv.FormatUint(uint64(frequency.color.g/0x101), 16))
				hexB := fmt.Sprintf("%02s", strconv.FormatUint(uint64(frequency.color.b/0x101), 16))
				csvdata[i+1] = "#" + hexR + hexG + hexB
			}
			select {
			case <-done:
				return
			case topColorsStream <- csvdata:
			}
		}
	}()
	return topColorsStream
}

// getThreeMostFrequent returns slice of three most frequenting colors from colorFreq in argument.
func getThreeMostFrequent(colorFreq map[colorRGB]int) []frequency {
	frequencies := make([]frequency, 0, len(colorFreq))
	for k, v := range colorFreq {
		frequencies = append(frequencies, frequency{
			color: k,
			count: v,
		})
	}
	sort.Slice(frequencies, func(i, j int) bool {
		return frequencies[i].count > frequencies[j].count
	})
	return frequencies[:3]
}

func fanIn(done <-chan interface{}, channels ...<-chan []string) <-chan []string {
	var wg sync.WaitGroup
	multiplexedStream := make(chan []string)

	wg.Add(len(channels))
	for _, c := range channels {
		go func(c <-chan []string) {
			defer wg.Done()
			for i := range c {
				select {
				case <-done:
					return
				case multiplexedStream <- i:
				}
			}
		}(c)
	}

	go func() {
		wg.Wait()
		close(multiplexedStream)
	}()

	return multiplexedStream
}

type frequency struct {
	color colorRGB
	count int
}

type colorRGB struct {
	r uint32
	g uint32
	b uint32
}

type pictFile struct {
	pict []byte
	url  string
}

var numCPU = runtime.NumCPU()
